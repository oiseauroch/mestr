FROM debian:stretch-slim

ARG Boost_Version_P=1.62.0
ARG Boost_Version_U=1_62_0
ARG Pcap_Version=1.8.1

ENV CXX=arm-linux-gnueabihf-g++
ENV CC=arm-linux-gnueabihf-gcc

RUN apt-get -qq update && apt-get install -y --no-install-recommends \
	cmake g++-arm-linux-gnueabihf git make ca-certificates\
# command needed for compiling boost, pcap and pcapplusplus
	&& rm -rf /var/lib/apt/lists/*

# boost compilation and installation
RUN  apt-get -qq update && apt-get install -y \
	wget bzip2 python-dev bison flex && \
	wget https://sourceforge.net/projects/boost/files/boost/$Boost_Version_P/boost_$Boost_Version_U.tar.bz2/download && \
	mv download boost.tar.bz2 && \
	tar -xjvf boost.tar.bz2 && \
	cd boost_$Boost_Version_U && \
	echo " using gcc : arm : arm-linux-gnueabihf-g++ ;" >> $HOME/user-config.jam && \
	./bootstrap.sh --with-libraries=system && \
	./b2 && \
	cp stage/lib/* /usr/arm-linux-gnueabihf/lib/ && \
	cp -r boost /usr/arm-linux-gnueabihf/include/ && \
	cd .. && \
	rm -r boost* $HOME/user-config.jam && \
	apt-get purge -y wget bzip2 python-dev bison flex &&\
	apt-get autoremove -y --purge && \
        rm -rf /var/lib/apt/lists/*

# pcap cross-compilation
RUN  apt-get -qq update && apt-get install -y --no-install-recommends \
	wget bzip2 flex bison && \
	wget http://www.tcpdump.org/release/libpcap-$Pcap_Version.tar.gz && \
	tar -xvf libpcap-$Pcap_Version.tar.gz && \
	cd libpcap-$Pcap_Version && \
	cmake . && \
	make && \
	mv libpcap.so /usr/arm-linux-gnueabihf/lib/ && \
	cp -r  pcap /usr/arm-linux-gnueabihf/include/ && \
# for backward compatibility
	cp pcap.h /usr/arm-linux-gnueabihf/include/ && \
	cd .. && \
	rm -r libpcap* && \
	apt-get purge -y wget flex bison && \
	apt-get autoremove -y --purge && \
        rm -rf /var/lib/apt/lists/*


#compile pcapPlusPlus
RUN  apt-get -qq update && apt-get install -y --no-install-recommends \
	wget && \
	wget https://gitlab.com/oiseauroch/mestr/raw/master/installPcapPlusPlusCrossComp.sh && \
	chmod +x installPcapPlusPlusCrossComp.sh && \
	./installPcapPlusPlusCrossComp.sh && \
	rm ./installPcapPlusPlusCrossComp.sh && \
	apt-get purge -y wget && \
	apt-get autoremove -y --purge && \
        rm -rf /var/lib/apt/lists/*

