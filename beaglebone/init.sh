#!/bin/bash
_LIST_FUNCTIONS=("disable_server" "install_boost" "install_cmake" "set_aliases" "update_sys" "update_uboot" "urxvt_support")

function usage(){
  printf "update the bone and initialise configuration\n"
  printf " usage : "
  printf "bbinit [option] script to init [default all]  \n"
  printf "OPTIONS : \n"
  printf "\t -h --help       : show this message.\n"
  printf "\t -a --all   : set up all init scripts. you can still add specific scripts as arguments but for disabling them\n"
 }

disable_server()
{
  is_root
  sudo systemctl disable apache2
  sudo systemctl disable bonescript.socket
  sudo systemctl disable bonescript.service
}

install_boost()
{
 is_root
 sudo apt install libboost-dev libboost-system-dev -y
}

install_cmake()
{
 is_root
 sudo apt install cmake -y
}

update_sys()
{
 is_root
 sudo apt update -y
 sudo apt upgrade -y
}

update_uboot()
{
  is_root
  cd /opt/scripts/tools
  git pull
  cd developers/
  ./update_bootloader.sh
}

set_aliases() 
{
 echo "uart() { screen /dev/ttyS$1 115200 8n1; }" >> $HOME/.bashrc
 echo "alias uEnv='sudo nano /boot/uEnv.txt'" >> $HOME/.bash_aliases
}

urxvt_support()
{
  is-root
  sudo apt install ncurses-term -y
}
#-------------------- tools functions-------------------

is_root()
{
  if [ "$EUID" -ne 0 ]
    then echo "Please run as root"
    exit 1;
  fi
}

list() 
{
 echo ${_LIST_FUNCTIONS[@]}
 exit 0
}

set_all()
{
all=yes;
}

 OPTS=$(getopt -o a,h,l -l help,all,list -- "$@" )
 eval set -- "$OPTS"

 while true ; do
     case $1 in
         -h) usage;
             exit 0;;
         --help) usage;
             exit 0;;
         -a) set_all ; shift ;;
         --all) set_all ; shift ;;
	 -l) list;;
	 --list) list;;
         --) shift ; break;;
     esac
done
if [ "$all" == "yes" ] ; then
  l_func=$(comm -13 <(printf "%s\n" $@ | sort) <(printf "%s\n" ${_LIST_FUNCTIONS[@]}))
else
 l_func=$*
fi
for arg in $l_func
do
  echo "executing $arg"
  "$arg";
done
echo "INIT FINISHED"
exit 0
