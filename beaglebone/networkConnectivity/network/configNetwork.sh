#!/bin/bash

function listInterface() # listInterface returnInterfaceList nbInterfaces
# Makes a list of the available ethernet interface, and count them
{
    local -n interfaces=$1
    local -n nbInterfaces=$2
    interfaces=$(ls /sys/class/net/ | grep -o "e.*") # The script can be used to select also wifi interface by replacing the grep expression with "[ew].*"

    nbInterfaces=$(echo -n "$interfaces" | grep -c '^')
}
function askInterfaceIfNeeded() # askInterfaceIfNeeded interfaceList interfaceToChoose interfaceChosen message
{
    local list=$1
    local -n interface=$2
    local -n chosenInterface=$3
    local message=$4
    if [ $interface = "__UNDEFINED__ " ] || [ $interactive == 2 ] || ! checkInterfaceExists $interface || [ $interface == $chosenInterface ]
    then
        echo "$message"
        select interface in ${list}
        do
            if [ "$interface" != "" ]
            then
                break;
            fi
            echo "Please choose one of the proposed interfaces."
        done
    fi
}
function selectInterface() # selectInterface parameters
# If both interfaces are not defined (or don't exist), or if both interfaces are the same, asks for the missing informations.
{
    local -n params=$1
    askInterfaceIfNeeded "${params[interfaces]}" params[wanInterface] params[lanInterface] "Select the interface to use as WAN interface : "
    
    local interfacesNoWan=${params[interfaces]//${params[wanInterface]}/} #creates a temporary list wich does not contain the interface chosen as wan interface.
    askInterfaceIfNeeded "$interfacesNoWan" params[lanInterface] params[wanInterface] "Select the interface to use as LAN interface : "
}
function askVariableIfNeeded() # askVariableIfNeeded variable "$message"
# Prompts the user for a value using the message, if the value is undefined or the interactivity level is 2.
{
    local -n variable=$1
    
    if [ "$variable" == "__UNDEFINED__" ] || [ "$interactive" == 2 ]; then
        read -p "$2" variable
    fi
}
function askYesNo() # askYesNo variable "$message"
# Prompts the user for yes or no using the message.
{
    local -n var=$1
    local message=$2
    local answer="__UNDEFINED__"
    while true; do
        askVariableIfNeeded var "$2 (yes/no) : "
        case $var in
            [Yy]* | 1 )
                var=1
                break
                ;;
            [Nn]* | 0 )
                var=0
                break
                ;;
            *)
                echo "Please enter yes or no."
                var="__UNDEFINED__"
                ;;
        esac
    done
}
function getMacWithoutColumn() # getMacWithoutColumn ifaceName
{
    local interface=$1
    echo $(sed "s/://g" /sys/class/net/${interface}/address) # Gets the mac address from the interface, and replace the columns with nothing.
}
function setIfaceLAN() # setIfaceLAN parameters
{
    local -n params=$1
    local lanInterface=${params[lanInterface]}
    local lanInterfaceHaddr=$(getMacWithoutColumn $lanInterface) # Gets the mac address without its column to build the service name.
    local lanServiceName="ethernet_${lanInterfaceHaddr}_cable"

    echo "Setting $lanInterface static IP"
    echo "sudo connmanctl config $lanServiceName --ipv4 manual ${params[lanIPAddr]} ${params[lanNetmask]} ${params[lanGateway]}"
}
function setIfaceWAN() # setIfaceWAN $ifaceName
{
    local wanInterface=$1
    local wanInterfaceHaddr=$(getMacWithoutColumn $wanInterface) # Gets the mac address without its column to build the service name.
    local wanServiceName="ethernet_${wanInterfaceHaddr}_cable"

    echo "Setting $wanInterface to use DHCP"
    echo "sudo connmanctl config $wanServiceName --ipv4 dhcp"
}
function configureDHCPServer() # configureDHCPServer parameters
# Fills the placeholders of the template dnsmas config file using the parameters, and replace the previous dnsmasq.conf file by this new one.
{
    local -n params=$1
    
    
    echo "Setting up the dhcp serveur on ${params[lanInterface]}, with : "
    printf "\tDhcp range : %s\n" "${params[minDhcpIP]}-${params[maxDhcpIP]}"
    printf "\tLease time : %s\n" "${params[leasetime]}"
    printf "\tDomain : %s\n" "${params[domain]}"
    printf "\tisAuthoritative : %s\n" "${params[isAuthoritative]}"
    printf "\tDefault gateway activated : %s\n" "${params[defaultGatewayActivated]}"

# Filling the template config file :
    sed -e "s/{INTERFACE}/${params[lanInterface]}/" -e "s/{MINDHCPIP}/${params[minDhcpIP]}/" -e "s/{MAXDHCPIP}/${params[maxDhcpIP]}/" -e "s/{LEASETIME}/${params[leasetime]}/" -e "s/{DOMAIN}/${params[domain]}/" -e "s/{LANIPADDR}/${params[lanIPAddr]}/" "${DIR}/dnsmasq.conf.template" > "${DIR}dnsmasq.conf.temp"

    if [ ${params[isAuthoritative]} == 1 ]
    then
        sed -i '/dhcp-authoritative/s/^#*//' "${DIR}dnsmasq.conf.temp"
    fi
    if [ ${params[defaultGatewayActivated]} == 0 ]
    then
        sed -i '/dhcp-option=3/s/^#*//' "${DIR}dnsmasq.conf.temp"
    fi
# Setting the right ownership and rights :
    chmod 0644 "${DIR}dnsmasq.conf.temp"
    sudo chown root:root "${DIR}dnsmasq.conf.temp"
    sudo mv -b -S .old "${DIR}dnsmasq.conf.temp" "${dnsmasqConfPath}dnsmasq.conf"
# Checking the configuration before restarting the service :
    if ! /usr/sbin/dnsmasq --test
    then
        echo "invalid dnsmasq.conf generated."
        sudo mv "${dnsmasqConfPath}dnsmasq.conf.old" "${dnsmasqConfPath}dnsmasq.conf"
        exit 1
    else # If the config is OK, restart the service. 
        echo "Restarting dnsmasq."
        echo "sudo systemctl restart dnsmasq.service"
    fi
}
function choiceDhcpServer() # choiceDhcpServer paramList
# If the server must be configured, configures the server.
{
    local -n paramList=$1
    if [[ ${paramList[configureDnsmasq]} = 1 ]]
    then
        configureDHCPServer paramList
    fi    
    
}
function checkInterfaceExists() # checkInterfaceExists ifaceName
# checks if the interface ifaceName exists
{
    local interface=$1
    [[ $interface != "" ]] && ls /sys/class/net/$interface/ &>/dev/null
}
function getMode() # getMode paramList
# sets the mode to lan or wan according to the provided informations.
{
    local -n params=$1
    if checkInterfaceExists ${params[lanInterface]}
        then
            params[mode]="lan"
        else
            if checkInterfaceExists ${params[wanInterface]}
            then
                params[mode]="wan"
            else
                params[mode]="__UNDEFINED__"
            fi
        fi
}
function setAndCheckParameters() # setAndCheckParameters parameters
# Gets the parameters from the config files, populates the parameter array with them.
# Checks if the parameters are defined, if they are not, asks for them or stops the script depending on the interactivity level.
{
    local -n paramlist=$1
    
    listInterface paramlist[interfaces] paramlist[nbInterfaces]
    if [ ${paramlist[nbInterfaces]} = 0 ]
    then
        echo "No ethernet interfaces detected, exiting."
        exit 0
    fi

    paramlist+=([lanInterface]="$(config_get lanInterface)")
    paramlist+=([wanInterface]="$(config_get wanInterface)")
    #Getting setting for the dnsmasq server
    paramlist+=([minDhcpIP]="$(config_get minDhcpIP)")
    paramlist+=([maxDhcpIP]="$(config_get maxDhcpIP)")
    paramlist+=([leasetime]="$(config_get leasetime)")
    paramlist+=([domain]="$(config_get domain)")
    paramlist+=([isAuthoritative]="$(config_get isAuthoritative)")
    paramlist+=([defaultGatewayActivated]="$(config_get defaultGatewayActivated)")
    paramlist+=([configureDnsmasq]="$(config_get configureDnsmasq)")
    paramlist+=([lanIPAddr]="$(config_get lanIPAddr)")
    paramlist+=([lanNetmask]="$(config_get lanNetmask)")
    paramlist+=([lanGateway]="$(config_get lanGateway)")

    if [ ${paramlist[nbInterfaces]} = 1 ]
    then
        getMode paramlist
        if (( $interactive >= 1 )); then
            while true; do
                askVariableIfNeeded paramlist[mode] "Please enter the mode for the interface ${paramlist[interfaces]}. (wan/lan) : "
                case ${paramlist[mode]} in
                    "wan" | "lan" )
                        break
                        ;;
                    *)
                        echo "Please enter lan or wan."
                        paramlist[mode]="__UNDEFINED__"
                        ;;
                esac
            done
        fi
    fi

    if (( $interactive >= 1 )); then
        
        if [[ ${paramlist[mode]} != "wan" ]]
        then
            if [ ${paramlist[lanIPAddr]} != "__UNDEFINED__" ]
            then
                askVariableIfNeeded paramlist[lanIPAddr] "Please enter the IP address of the Lan interface : "
                askVariableIfNeeded paramlist[lanNetmask] "Please enter the netmask : "
                askVariableIfNeeded paramlist[lanGateway] "Please enter the IP address of the gateway : "
            fi
            
            askYesNo paramlist[configureDnsmasq] "Configure the dnsmasq server ?"
            if [ ${paramlist[configureDnsmasq]} = 1 ] #If we don't want to configure the server, no need to ask for the parameters.
            then
                askYesNo paramlist[isAuthoritative] "Shall the DHCP server be authoritative ?"
                askYesNo paramlist[defaultGatewayActivated] "Shall the default gateway option be sent to the DHCP clients ?"
                askVariableIfNeeded paramlist[minDhcpIP] "Please enter the minimum IP address of the DHCP range : "
                askVariableIfNeeded paramlist[maxDhcpIP] "Please enter the maximum IP address of the DHCP range : "
                askVariableIfNeeded paramlist[leasetime] "Please enter the leasetime of the DHCP leases (in the same form as 12h) : "
                askVariableIfNeeded paramlist[domain] "Please enter the domain name : "
            fi
            if (( ${paramlist[nbInterfaces]} >1 )); then
                selectInterface paramlist
            fi
        fi
    else
        if [[ ( ${paramlist[configureDnsmasq]} != 0 && ( ${paramlist[configureDnsmasq]} = "__UNDEFINED__" || ${paramlist[minDhcpIP]} = "__UNDEFINED__" || ${paramlist[maxDhcpIP]} = "__UNDEFINED__" || ${paramlist[leasetime]} = "__UNDEFINED__" || ${paramlist[domain]} = "__UNDEFINED__" || ${paramlist[isAuthoritative]} = "__UNDEFINED__" || ${paramlist[defaultGatewayActivated]} = "__UNDEFINED__" ) ) # If configureDnsmasq is 0, no need to check the presence of the dhcp options.
        || ${paramlist[lanIPAddr]} = "__UNDEFINED__" || ${paramlist[lanNetmask]} = "__UNDEFINED__" || ${paramlist[lanGateway]} = "__UNDEFINED__" 
        || ( ${paramlist[nbInterfaces]} = 1 && ${paramlist[mode]} = "__UNDEFINED__" ) ]] # But we still check the others options
        then 
        # The script stops if ((A && B) || C || D), with :
        # A="the dhcp server must be configured"
        # B="a DHCP parameters is undefined"
        # C="any other parameter is undefined"
        # D="there is only one interface and its mode is undefined"

            echo "$0 : one or more requested parameters undefined, aborting."
            exit 1
        fi
        if (( ${paramlist[nbInterfaces]} >1 )); then
            if ! checkInterfaceExists ${paramlist[lanInterface]}
            then
                echo "No interface found with name ${paramlist[lanInterface]}"
                exit 1
            fi
            if ! checkInterfaceExists ${paramlist[wanInterface]}
            then
                echo "No interface found with name ${paramlist[wanInterface]}"
                exit 1
            fi
            if [ ${paramlist[lanInterface]} == ${paramlist[wanInterface]} ]
            then
                echo "Wan and lan interfaces can't be the same."
                exit 1
            fi
        fi
        
    fi

}
########## Utilitary function ########## 
# Those two fonctions are used to read the config file : 
config_read_file() {
    (grep -E "^${2}=" -m 1 "${1}" 2>/dev/null || echo "VAR=__UNDEFINED__") | head -n 1 | cut -d '=' -f 2-;
}
config_get() {
    val="$(config_read_file $configPath "${1}")";
    if [ "${val}" = "__UNDEFINED__" ]; then
        val="$(config_read_file $defaultConfigPath "${1}")";
    fi
    if [ "${val}" = "" ]; then # we consider empty values as undefined.
        val="__UNDEFINED__";
    fi
    printf -- "%s" "${val}";
}
# This function displays a bit of help :
function usage(){
    local bold=$(tput bold)
    local normal=$(tput sgr0)
    printf "This script configures the interface(s) to use them as a lan or wan interface.\n"
    printf "Usage :\n"
    printf "\t$0 [-m mode][-c pathToConfig]\n\n"
    printf "\t${bold}-m interactivityLevel, --mode=interactivityLevel${normal}\n"
    printf "\t\tSets the interactivity level of the script: ${bold}0${normal}: fully automatic, ${bold}1${normal}: half-automatic, ${bold}2${normal}: fully interactive. (default : 1)\n"
    printf "\t\tIn ${bold}fully automatic${normal} mode, a config file MUST be used, at least to precise the interfaces to be used.\n"
    printf "\t\tIn ${bold}half-automatic${normal} mode, if no config file is provided, the script will only ask for the interface to use. You can make it ask for others parameters by specifying a config file containing these parameters, with empty value. They will be asked each time you run the script with this config file..\n"
    printf "\t\tIn ${bold}fully interactive${normal} mode, the script will ask for all the parameters used during the configuration.\n"
    
    printf "\n\t${bold}-c pathToConfig, --config=pathToConfig${normal}\n"
    printf "\t\tDefines the path to a custom config file. If no path is specified, the custom config file will be seeked in the same location as the script\n"
}
######################### End of functions definition #########################

# variables default values :
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )" # get script directory
defaultConfigPath="$DIR/config.cfg.defaults"
configPath="$DIR/config.cfg"
dnsmasqConfPath="$DIR/"
interactive=1

args=$(getopt -o "m:c:h" -l mode:,config: -- "$@")
[ $? -eq 0 ] || { 
    echo "Incorrect options provided"
    exit 1
}
eval set -- "$args"

while [ $# -ge 1 ]; do
        case "$1" in
                --)
                    # No more options left.
                    shift
                    break
                   ;;
                -m | --mode )
                    interactive="$2"
                    shift
                    ;;
                -c | --config )
                    configPath="$2"
                    if [ ! -s $configPath ]; then
                        echo "config file doesn't exist"
                        exit 1
                    fi
                    shift
                    ;;
                -h)
                    usage
                    exit 0
                    ;;
        esac

        shift
done

declare -A parameters

setAndCheckParameters parameters

if [ ${parameters[nbInterfaces]} = 1 ]
then
    case ${parameters[mode]} in
        "wan" )
            echo "Setting interface as Wan interface"
            setIfaceWAN ${parameters[interfaces]}
            ;;
        "lan" )
            echo "Setting interface as Lan interface"
            setIfaceLAN parameters
            choiceDhcpServer parameters
            ;;
        * )
            echo "Lan or Wan mode unspecified"
            exit 1
            ;;
    esac
else
    setIfaceWAN ${parameters[wanInterface]}
    setIfaceLAN parameters
    choiceDhcpServer parameters
fi
