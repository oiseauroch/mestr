#!/bin/bash

function usage(){
  printf "This script configures routing between 2 interfaces.\n"
  printf "Usage :\n"
  printf "\t $0 -w wanInterface -l lanInterface [-pn]\n"
  printf "\t \e[4mwanInterface\e[0m is the interface connected to the wan.\n"
  printf "\t \e[4mlanInterface\e[0m is the interface connected to the lan.\n"
  printf "\t \e[4m-p\e[0m makes the change persistent\n"
  printf "\t \e[4m-n\e[0m prevent the installation of iptables-persistent. (It prevents having to wait for apt-get update to finish for nothing if the package is already installed.)\n\n"
  printf "As you need root priviledges to change routing table configurations, this script must be run as root.\n"
}

is_root()
{
  if [ "$EUID" -ne 0 ]
    then echo "Please run as root"
    exit 1;
  fi
}

wan=""
lan=""
persistent=0
install=1
args=$(getopt -o phnw:l: -- "$@")
[ $? -eq 0 ] || { 
    echo "Incorrect options provided"
    exit 1
}


eval set -- "$args"

while [ $# -ge 1 ]; do
        case "$1" in
            --)
                shift
                break
                ;;
            -w )
                wan="$2"
                shift
                ;;
            -l )
                lan="$2"
                shift
                ;;
            -p )
                persistent=1
                ;;
            -n )
                install=0
                ;;
            -h )
                usage
                exit 0
                ;;
        esac

        shift
done



if [[ $lan == "" ]] || [[ $wan == "" ]]; then
    echo "The lan and wan interfaces must be specified."
    exit 0
fi
is_root
## For a non persistent connexion :
sh -c "echo 1 > /proc/sys/net/ipv4/ip_forward"
iptables -A FORWARD -i "$lan" -o "$wan" -j ACCEPT
iptables -A FORWARD -i "$wan" -o "$lan" -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -t nat -A POSTROUTING -o "$wan" -j MASQUERADE

## To make it persistant :
if [ $persistent == 1 ]; then
    if  [ "$(grep -e "net.ipv4.ip_forward=1" /etc/sysctl.conf)" != "net.ipv4.ip_forward=1" ]; then
        echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf
    fi
    if [ $install = 1 ]
    then
        apt-get -y update
        apt-get -y install iptables-persistent
    fi
    iptables-save > /etc/iptables/rules.v4
fi
