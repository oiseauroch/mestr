#!/bin/bash

function usage(){
    printf "This script enables or disables routing between 2 interfaces.\n"
    printf "Usage :\n"
    printf "\t $0 -s state\n"
    printf "\t \e[4mstate\e[0m is either \e[1menabled\e[0m or \e[1mdisabled\e[0m.\n\n"
    printf "As you need root priviledges to change routing table configurations, this script must be run as root.\n"
}

is_root()
{
  if [ "$EUID" -ne 0 ]
    then echo -e "Please run as root"
    exit 1;
  fi
}

state=""
args=$(getopt -o "s:h" -- "$@")

eval set -- "$args"

while [ $# -ge 1 ]; do
        case "$1" in
                --)
                    # No more options left.
                    shift
                    break
                    ;;
                -s)
                    state="$2"
                    shift
                    ;;
                -h)
                    usage
                    exit 0
                    ;;
        esac

        shift
done
if  [ "$state" != "enabled" ] && [ "$state" != "disabled" ] 
then
  echo "you must specifiy 'enabled' or 'disabled'"
  usage
  exit 0
else
    is_root
    if [ -s "/etc/iptables/rules.v4" ]
    then
        if [ "$state" == "enabled" ]
        then
            sh -c "echo 1 > /proc/sys/net/ipv4/ip_forward"
            iptables-restore /etc/iptables/rules.v4
        else
            sh -c "echo 0 > /proc/sys/net/ipv4/ip_forward"
            iptables --flush
            iptables -t nat --flush
        fi
    else
        echo "Please configure the IP forwarding and make it persistent using the setupRouting script."
    fi
fi
