# Methods that didn't work to enable UART
There is a good summary of the issue [here](https://unix.stackexchange.com/questions/444275/failing-to-enable-uarts-on-beaglebone-black)

## Modifiyng uEnv.txt (which has an unsure location) :

```
/boot/uboot/uEnv.txt
/boot/uEnv.txt
```

adding this line : `optargs=capemgr.enable_partno=BB-UART1,BB-UART2,BB-UART3,BB-UART5`
or this lines : 
```
cape_disable=bone_capemgr.disable_partno=BB-BONELT-HDMI,BB-BONELT-HDMIN
cape_enable=bone_capemgr.enable_partno=BB-UART1,BB-UART2,BB-UART4.BB-UART5
###Overide capes with eeprom
uboot_overlay_addr0=/lib/firmware/BB-UART1-00A0.dtbo
uboot_overlay_addr1=/lib/firmware/BB-UART2-00A0.dtbo
uboot_overlay_addr2=/lib/firmware/BB-UART4-00A0.dtbo
uboot_overlay_addr3=/lib/firmware/BB-UART5-00A0.dtbo
```
## Modifiyng the capemgr file

```
/etc/default/capemgr
```
adding the lide : `CAPE=BB-SPI-01,BB-UART1,BB-UART2,BB-UART4`

## using a device file :

```
echo BB-UART4 > /sys/devices/platform/bone_capemgr/slots
```
**This one broke ssh on the following reboot**, but it came back later.
