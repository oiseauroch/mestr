# BeagleBone

## Flash SD card

The board want to boot in a system with to partition. In order to correct it, rename the file : `bbb-uEnv.txt` to `uEnv.txt`

## Serial port

### USB (So easy)
```
sudo screen /dev/ttyUSBX 115200 8n1
```


### debug port
```
  _________________
.|GD|  |  |RX|TX|  |
 |__|__|__|__|__|__|
```


### Flash the eMMc

 * Open `/boot/uEnv.txt`
 * Uncomment the line : `#cmdline=init=/opt/scripts/tools/eMMC/init-eMMC-flasher-v3.sh`
 * poweroff the card and then power on again.
 * Wait until the board power off.
 * reboot without SD card

### UART PORTS
By default only UART1 is activated. There is plenty of way to activate other UART ports but only 1 seems to work with the SD card : recompiling the device 
tree.

#### recompiling DT (SD Card)

 * get the file `am335x-boneblack.dtb` in `/boot/dtbs/4.14.49-ti-r54/`
 * make a backup of the file to be sure
 * decompile it with device-tree-compiler : `sudo dtc -I dtb -O dts -o am335x-boneblack.dts am335x-boneblack.dtb
 * modify the file by adding to set the pinout of the uart (example for UART4) : 
```bash
pinmux_uart4_pins {
     pinctrl-single,pins = <0x070 0x26 0x074 0x06>;
     phandle = <0x64>; 
};
```
 Just after the `pinmux_uart0_pins` part.

The *phandle* line should use an unused part of the memory. Unfortunately, 
`0x64` is currently used by a led that should be removed from the device 
tree in order that compilation pass.

all the pinout can be found on [armhf](http://www.armhf.com/beaglebone-black-serial-uart-device-tree-overlays-for-ubuntu-and-debian-wheezy-tty01-tty02-tty04-tty05-dtbo-files/)

 * and activate the uart une the same file : 
```bash
serial@48022000 {
			compatible = "ti,omap3-uart";
			ti,hwmods = "uart5";
			clock-frequency = <0x2dc6c00>;
			reg = <0x48022000 0x2000>;
			interrupts = <0x49>;
			status = "disabled"; <- set "okay"
			linux,phandle = <0x19>;
			phandle = <0x19>;
		};
```
Note that number of UART is increased by une here, so for UART4, modify 
UART5.

 * compile your modifies file with `sudo dtc -I dts -O dtb -o am335x-boneblack.dtb am335x-boneblack.dts`

#### Use [beaglebone-universal-io](https://github.com/cdsteinkuehler/beaglebone-universal-io)

With the OS flash on the eMMc, gpio are working fine as expected. In this case, just use the command : 
```bash
config-pin P[8-9]_XX uart 
```
for both RX and TX ports


## ip address

```
board n°24 : 10.3.4.113
board n°40 : 10.3.4.121
```

## enable/disable ethernet interface

By default, the internet interface is not declared in `/etc/network/interface`, preventing the `ifupdown` command from working.

To make the use of this command to enable and disable the eth0 interface possible, add or uncomment the lines :
```bash
auto eth0
iface eth0 inet dhcp
```
in `/etc/network/interface`.

Then you can use `sudo ifdown eht0` to disable the eth0 interface and `sudo ifdown eht0` to inable it.
Be sure to have an other way to connect to the beaglebone before disabling the interface !
