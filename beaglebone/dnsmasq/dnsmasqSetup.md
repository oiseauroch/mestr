# Setting up a dhcp server, and a DNS server using dnsmasq

## Setting up a static ip on the right interface.
The beaglebone debian distro uses connman as network manager. 

You can either use connman to set a static ip address

sudo connmanctl config <service> --ipv4 manual <ip_addr> <netmask> <gateway> --nameservers <dns_server>

In our case :
```
sudo connmanctl config ethernet_1cba8c97df5f_cable --ipv4 manual 192.168.0.1 255.255.255.0 192.168.0.1
```
To enable again the dhcp client, we can use :
```
sudo connmanctl config ethernet_1cba8c97df5f_cable --ipv4 dhcp

```
Or blacklist the interface that you want to use for the dhcp server (probably not the best idea), so that it is not managed by connman, and can have a fixed IP adress defined in `/etc/network/interfaces`.

Add
```
NetworkInterfaceBlacklist=<Name of the interface>,SoftAp0,usb0,usb1
```
in `/etc/connman/main.conf` to blacklist it, then add

```
auto <Name of the interface>
iface <Name of the interface> inet static
    address 192.168.1.2/24
    gateway 192.168.1.1
```
in `/etc/network/interfaces` to set the static IP address.

## Configuring the server
A commented configuration file can be found in `example.dnsmasq.conf`. It is very detailed, but we do not need all of this for now.

Our config file is named `dnsmasq.conf`, and contains less options, but still contains all the comments for these options.


The server should be put in authoritative mode, to force new clients to get an IP from it. It is particularly needed in the case of clients taking a default IP at boot if no DHCP server is found.
To do so, uncomment the `dhcp-authoritative` line of `dnsmasq.conf`
