

# Guidelines

```
#ifndef MY_CLASS_H
#define MY_CLASS_H 

#include <...>

class MyClass
{
public:
    MyClass(T vbar);
    type DoSomething(type varFoo);
    inline T GetBar();
    void SetBar(T bar);
    bool IsAction();
    ~MyClass();

private:
    T bar;
};

#endif // MY_CLASS_H
```

```
#include "MyClasss.h"

MyClass::MyClass(T vbar):bar(vbar)
{
    doSommething...
}



```