function usage(){

    printf "crosscompile the project for beaglebone\n"
    printf "usage : crossComp.sh [OPTIONS] [MAKE RECIPE]\n"
    printf "\t -s --send : send binaries to specific ip address \n"
    printf "\t -d --destination : change build folder [cross-compilation]\n"
    printf "\t --cmake-args : add cmake extra arguments\n"
    printf "\t -h --help : display this message and exit\n"
}


OPTS=$( getopt -o h,s:,d: -l help,send:,destination:,cmake-args -- "$@" )

_CMAKE_ARGS="-DCMAKE_TOOLCHAIN_FILE=$PWD/CMake-BB-toolchain"
_DEST="cross-compilation"

eval set -- "$OPTS"

    # call each fonction if option is present
    while true ; do
    case $1 in

    -h|--help) usage;
        exit 0;;

    -s|--send)  _SEND="true";
	ip=$2;
        shift 2;;

    -d|--destination) _DEST=$2;
        shift 2;;

    --cmake-args)  _CMAKE_ARGS="$_CMAKE_ARGS $2";
                shift 2;;

    --) shift ; break;;

    \?)
    echo "Invalid option: -$OPTARG" >&2
    exit 2;;

    esac
done
_PWD=$PWD

[[ -d $_DEST ]] || ( echo "create destination directory" &&  mkdir -p $_DEST )
cd $_DEST && cmake $_CMAKE_ARGS $_PWD  && make $1 || exit 1
if [ "$_SEND" == "true" ] ; then
    scp -r bin/$1 debian@$ip:bin/ || scp -r bin/* debian@$ip:bin/ || exit 2
fi
echo "done"
exit 0
