#include "DhcpSniffer.h"
#include <iomanip>
bool AlternativeHandler(pcpp::RawPacket* packet, pcpp::PcapLiveDevice* dev, void* cookie) {
    double time = packet->getPacketTimeStamp().tv_sec;
    time +=(static_cast<float>(packet->getPacketTimeStamp().tv_usec)/static_cast<float>(1000000));
    std::cout << "Packet arrived at : "<< std::fixed << std::setw(20)
        << std::setprecision(6)<< time <<"s"<< std::endl;
    pcpp::Packet parsedPacket(packet); //parsing the packet
    pcpp::DhcpLayer* dhcp = parsedPacket.getLayerOfType<pcpp::DhcpLayer>();//getting the dhcp DhcpLayer
    if(dhcp) //if the packet is a DHCP packet, process
    {
        std::cout << "Packet type : " << dhcp->getMesageType() << std::endl;
        std::cout << "YIADDR : "<< dhcp->getYourIpAddress().toString() << std::endl;
        
        std::cout << "The included options are :" << std::endl ;
        
        auto option = dhcp->getFirstOptionData();
        while (option->getType()!=pcpp::DhcpOptionTypes::DHCPOPT_END)// display the options list
        {
            std::cout << option->getType() << std::endl ;
            option=dhcp->getNextOptionData(option);
        }
        
        
        if (dhcp->getOptionData(pcpp::DhcpOptionTypes::DHCPOPT_DHCP_PARAMETER_REQUEST_LIST)) // if the packet contains a parameter request list, display all the requested parameters.
            {
                std::cout << "ParamReqList : " << std::endl ;
                pcpp::DhcpOptionData* data = dhcp->getOptionData(pcpp::DhcpOptionTypes::DHCPOPT_DHCP_PARAMETER_REQUEST_LIST);
                int len = data->getLength();
                for (int i=0;i<len;i++)
                {
                    std::cout << +data->value[i] <<std::endl;
                }
            }
        
        
        if (dhcp->getMesageType()== pcpp::DhcpMessageType::DHCP_ACK)
            return true;
    }
    
    return false;
}

int main(int argc, char* argv[]){
    DhcpSniffer sniff;
    
    if (argc == 2)
    {
        std::string interfaceName(argv[1]);
        //Can be used like this :
        std::cout << "Default handler" << std::endl ;
        sniff.Run(interfaceName, 10);
        std::vector<pcpp::Packet> listPackets = sniff.GetPackets();
        for (auto packet : listPackets)
        {
            auto dhcp = packet.getLayerOfType<pcpp::DhcpLayer>();
            if (dhcp)
            {
                std::cout << "Type : " << dhcp->getMesageType() << std::endl ;
            }
        }
        //Or with an alternative handler like this :
        std::cout << "Alternative handler" << std::endl ;
        
        sniff.Run(interfaceName,60, AlternativeHandler);
    }else
    {
        std::cout << "Bad number or arguments. Expected : 1, got : " << argc-1 <<"."<< std::endl ;
        std::cout << "Usage : " << argv[0] << " interfaceName" << std::endl ;        
    }
    
    
}
