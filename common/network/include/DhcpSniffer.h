#ifndef DHCPSNIFFER_H
#define DHCPSNIFFER_H
#include <iostream>
#include <list>
#include <vector>
#include <pcapplusplus/PcapLiveDeviceList.h>
#include <pcapplusplus/PlatformSpecificUtils.h>
#include <pcapplusplus/DhcpLayer.h>
/**
 * @brief class to capture DHCP packets from the network.
 *  
 */

class DhcpSniffer
{
public :
    DhcpSniffer();
    ~DhcpSniffer()=default;
    void Run(std::string interface, unsigned int time=60,pcpp::OnPacketArrivesStopBlocking function = &DhcpSniffer::PacketHandler, void* cookie = nullptr);
    inline std::vector<pcpp::Packet>  GetPackets(){return packets;};
private:
    std::vector<pcpp::Packet> packets;

    static bool PacketHandler(pcpp::RawPacket* packet, pcpp::PcapLiveDevice* dev, void* cookie);
};

#endif // DHCPSNIFFER_H
