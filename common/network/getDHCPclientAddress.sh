#!/bin/bash

function usage(){
  printf "This script captures the ACKNOWLEDGE DHCP packet, and gets the IP address of the client\n"
  printf "Usage :\n"
  printf "\t $0 -i interface -t time\n"
  printf "\t \e[4minterface\e[0m is the interface you want to listen on.\n"
  printf "\t \e[4mtime\e[0m can be given is seconds (s, default), minutes (m), hours (h) or days (d).\n"
  printf "As dhcpdump needs root privileges, this script must be run as root.\n"
}

is_root()
{
  if [ "$EUID" -ne 0 ]
    then echo "Please run as root"
    exit 1;
  fi
}

interface=""
timeout=""

args=$(getopt -o "i:t:h" -- "$@")

eval set -- "$args"

while [ $# -ge 1 ]; do
        case "$1" in
                --)
                    # No more options left.
                    shift
                    break
                   ;;
                -i)
                        interface="$2"
                        shift
                        ;;
                -t)
                        timeout="$2"
                        shift
                        ;;
                -h)
                        usage
                        exit 0
                        ;;
        esac

        shift
done

if  [ "$interface" = "" ] || [ "$timeout" = "" ] 
then
  echo "-i and -t must be specified."
  usage
  exit 0
else
  is_root
  currentDate=$(date --rfc-3339=seconds | sed s/' '/_/)
  outputReport="dhcpdump_report-$currentDate.txt" #create the name of the report file, using the current date and time.
  dhcpdump -i $interface > "$outputReport" &
  PID=$(ps -C dhcpdump -o pid= | grep -o "[[:digit:]]*") # get the PID of dhcpdump in $PID
  sleep $timeout
  kill -s 15 $PID # stop dhcpdump by sending it SIGTERM.
  wait $PID &>/dev/null # capture the termination message of dhcpdump.
  ipaddresses=$(mktemp -p ./ ipaddrXXX) # generate a temporary file for IP addresses
  
  
  grep -B 8 "DHCPACK" $outputReport| grep "YIADDR" | grep -v "0.0.0.0"  | grep -o -P -e "\b(?:(25[0-5]|2[0-4][0-9]|[01]?[0-9]?[0-9])\.(25[0-5]|2[0-4][0-9]|[01]?[0-9]?[0-9])\.(25[0-5]|2[0-4][0-9]|[01]?[0-9]?[0-9])\.(25[0-5]|2[0-4][0-9]|[01]?[0-9]?[0-9]))\b" > $ipaddresses 
  #----- Explanation of the previous line : -----
  # Get the context of the DHCPACK packets contained in the report from dhcpdump.
  # Keep only the lines containing YIADDR (the address of the client).
  # Exclude the lines containing a "0.0.0.0" IP adress (not necessarily needed, but you never know, and there is a problem if there is such a line).
  # Keep only the IP addresses.
  # Put them in the temporary file.
  # ---------------------------------------------
  
  if [ "$(tail -n 1 $ipaddresses)" != "" ]
  then
    sort -o $ipaddresses -u $ipaddresses # remove duplicate addresses
    while read ip_address; do # ping all the IP addresses of the temporary file.
      ping -c 4 $ip_address &>> "ping_report-$currentDate.txt" #
    done < $ipaddresses
    cat $ipaddresses
  else
    echo "No DHCP client detected in requested time ($timeout), or error during the DHCP operation. See $outputReport for details."
  fi
  rm $ipaddresses
fi
