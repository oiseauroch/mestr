#include "DhcpSniffer.h"

/**
 * @brief Dhcpsniffer defines the default configuration for the sniffer.
 * 
 */
DhcpSniffer::DhcpSniffer()
{
    
}

/**
 * @brief Starts the sniffing, using the default packet handler.
 * 
 * @param interface interface on which to listen
 * @param time How long, in second, does the sniffing lasts (default : 60).
 * @param function Packet handler (default : the default packet handler)
 * @param cookie The context cookie, which is passed to the packed handler (default : nullptr, is replaced at runtime by the address of the packet vector.)
 */
void DhcpSniffer::Run(std::string interface, unsigned int time, pcpp::OnPacketArrivesStopBlocking function, void* cookie)
{
    packets.clear();
    pcpp::PcapLiveDevice* dev = pcpp::PcapLiveDeviceList::getInstance().getPcapLiveDeviceByName(interface);
    if (dev == NULL)
    {
        throw(std::runtime_error("Could not find interface with name "+interface));
    }
    if (!dev->open())
    {
        throw(std::runtime_error("Could not open device"));
    }
    dev->setFilter("udp port 67 or port 68");
    if (cookie == nullptr) {cookie = &packets;}//If the cookie is not specified, use the packet vector as a cookie.
    dev->startCaptureBlockingMode(function,cookie,time);
    dev->close();
    
}
/**
 * @brief Default packet handler : Converts all the packets in DHCP packets and adds them to the vector passed as a parameter.
 * 
 * @param packet Raw packet catched by the sniffer
 * @param dev Device on witch thesniffer is listening
 * @param cookie Context element, must be a pointer to a vector of packets
 * @return bool Always false to keep sniffing
 */
bool DhcpSniffer::PacketHandler(pcpp::RawPacket* packet, pcpp::PcapLiveDevice* dev, void* cookie)
{
    pcpp::Packet parsedPacket(packet);

    //std::cout << parsedPacket.toString() <<std::endl;
    auto vect = static_cast<std::vector<pcpp::Packet>*>(cookie);
    vect->push_back(parsedPacket);
    return false;
}


