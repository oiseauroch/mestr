# Network tests

## DHCP test

### Using dhcpdump (not advised):

The `getDHCPclientAdress.sh` script uses dhcpdump to capture the DHCP packets, and puts the result in a file named like this : `dhcpdump_report-<date>.txt`, automatically.

It focuses on the ACKNOWLEDGE packet, to get the IP address of the client, and pings it.

#### Usage :

```
getDHCPclientAdress.sh -i interface -t time
```
where *interface* is the interface dhcpdump listens on, and *time* is the delay during which dhcpdump is listening.

#### Outputs :
The script displays on screen the IP addresses of the client. 
It creates a `dhcpdump_report-<date>.txt`, containing all the packets captured by dhcpdump, for advanced analysis.
It also creates a `ping_report-<date>.txt`, which contains the result of the ping command.

### DhcpSniffer (more advised)

c++ class to sniff dhcp packets on a specific interface, and collect them for analysis.
It uses the pcap++ library, that provides an easy to use interface on top of `libpcap`.

The packet sniffer can call callback functions to process the packets as they arrive, or fill a vector with them for postponed analysis.
 
## mDNS test

Using `avahi-browse`, we can browse all the mDNS services registered on the LAN.

### Usage :

```
avahi-browse -altpk --resolve
```
The command browses all the available services, on all the interfaces. You may want to use
```
avahi-browse -altpk --resolve | grep "my_interface"
```
to limit the results to only one interface.

### Output

The command returns all the services, in an easy to parse format :

```
+;eth0;IPv4;Smappee2004000023;_ssh._tcp;local
+;eth0;IPv4;Smappee2004000023;_sftp-ssh._tcp;local
=;eth0;IPv4;Smappee2004000023;_ssllh._tcp;local;Smappee2004000023.local;192.168.0.55;22;
=;eth0;IPv4;Smappee2004000023;_sftp-ssh._tcp;local;Smappee2004000023.local;192.168.0.55;22;
```

The lines prefixed with a "=" are the ones that are resolved (we have the association of the host name and of the IP address).

By applying a RegEx to the output, it is easy to detect if the correct hostname is broadcasted, for the correct IP address.

