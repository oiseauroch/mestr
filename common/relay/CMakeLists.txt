#set(CMAKE_CXX_FLAGS "-w -lboost_system -pthread")
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin")



add_subdirectory(src) 
add_subdirectory(include)

include_directories(include ../lib/configPin)



# executable for testing Relay class
set(class src/Relay.cpp
		main-relay.cpp)

add_executable(relayClass ${class})
target_link_libraries(relayClass ConfigPin)
