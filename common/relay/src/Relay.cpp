#include "Relay.h"
#include <stdexcept>
#include <iostream>

/**
* @brief constructor of class in charge of managing relay
* perform the following actions :
*  - set output direction to the port
*  - set the relay to state off (false) according to the mode
* 
* @param gpio name of the output with this convention : P[8-9]_XX
* @param m system plug on normally open  (NO)[default] or normally close (NC)
*/
Relay::Relay(ConfigPin::PINS gpio, RELAY_MODE m)
{
    this->mode = m;
    this->pin = gpio;
    
    /*disactive relay by default
     * relay is activated if no current com from the control wire
     */
    if(mode == NC)
    {
        ConfigPin::writePin(this->pin, ConfigPin::PIN_VALUE::LOW);
    } else
    {
        ConfigPin::writePin(this->pin, ConfigPin::PIN_VALUE::HIGH);
    }
    this->currentState = false;
    
}

/**
* @brief get the current state of the relay (true-> on)
* 
* @return bool : current state
*/
bool Relay::getState()
{
    return this->currentState;
}

/**
* @brief set the current state of the relay
* 
* @param state (true -> on) 
*/
void Relay::setState(bool state)
{
    ConfigPin::PIN_VALUE val;
    if (this->mode==NC)
    {
        val = state?ConfigPin::PIN_VALUE::HIGH : ConfigPin::PIN_VALUE::LOW;
    } else
    {
        val = state?ConfigPin::PIN_VALUE::LOW : ConfigPin::PIN_VALUE::HIGH;
    }
    ConfigPin::writePin(pin, val);
    this->currentState = state;
}

/**
* @brief power off the relay
* 
*/
Relay::~Relay()
{
    this->setState(false);
}
