#ifndef RELAY_H
#define RELAY_H
#include <string>
#include "ConfigPin.h"
    


/**
* @brief class in charge of the relay 
* assign the relay to a specific pin
* then can power it on (state true) or off (state false)
* WHen the Realy object is destroyed, relay is by default power off
*/
class Relay
{
enum RELAY_MODE { 
        NO = 0,
        NC = 1};
public :
    Relay(ConfigPin::PINS gpio, RELAY_MODE mode= NO);
    ~Relay();
    void setState(bool state);
    bool getState();
private :
    ConfigPin::PINS pin ;
    RELAY_MODE mode;
    bool currentState;
};

#endif // RELAY_H
