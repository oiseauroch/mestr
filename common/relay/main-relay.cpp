#include "Relay.h"
#include <iostream>
#include <unistd.h>


int main(int argc, char*  argv[])
{
    ConfigPin::PINS port;
    std::cout << "port used : P9_15" << std::endl ;
    port=ConfigPin::PINS::P9_15;

    std::cout << "beginning with port " << port << std::endl ;

    // init relay
    Relay testRelay(port);
    sleep(3);
    std::cout << "set relay activated" << std::endl;
    testRelay.setState(true);
    sleep(3);
    std::cout << "set relay disactivated" << std::endl;
    testRelay.setState(false);
    sleep(3);
    std::cout << "finished" << std::endl ;
}
