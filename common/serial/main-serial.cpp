#include "Serial.h"
#include <iostream>
#include <unistd.h>

int i = 0;
  bool StringCallback(const std::string& str)
  {
      int pos = str.find("CPU");      
      if(pos >= 0)
      {
          std::cout << "-------------------I2C found--------------------" << std::endl ;
        return false;
    }
    return true;
}

bool BasicCallback(boost::asio::streambuf& buf, const boost::system::error_code& error, std::size_t nbBytes)
{
    if(!error) 
    { 
    std::string line;
    std::getline(std::istream(&buf), line); 
    std::cout << line << std::endl ;
    int pos = line.find("512");
    if(pos>= 1)
    {
        std::cout << "---------------------------512 found---------------------" << std::endl ;
        std::cout << "@@@@@nByte value : " << nbBytes << std::endl ;
        
        return false;
    }
    return true;
    } else{
        std::cout << "read error : " << error << " "  << error.message() << std::endl ;
        throw(boost::system::system_error(boost::system::error_code(error), "error while reading"));
    }
}

bool StringUntil(const std::string& str)
{
    std::cout << "£" << std::endl ;
    
        if (i++ < 10) return true ;
        
        
        std::cout << "____________STRING_UNTIL_FINISHED____________-" << std::endl ;
    return false;
}

bool BasicUntil(boost::asio::streambuf& buf, const boost::system::error_code& error, std::size_t nbBytes)
{
    std::string s(boost::asio::buffers_begin(buf.data()),
                  boost::asio::buffers_begin(buf.data()) + nbBytes);
    buf.consume(nbBytes);
    std::cout << s << std::endl ;
        if (i++ < 15) return true ;    
        std::cout << "____________BASIC_UNTIL_FINISHED_____________" << std::endl ;
        std::cout << "nByte value : " << nbBytes << std::endl ;
    return false;
}


int main(int argc, char*  argv[])
{
    Serial uart4(Serial::UARTS::UART4, 115200);
    uart4.SetTimeout(20, false);
    uart4.SetDisplay(true);
    
    std::cout << "___________read lines String___________" << std::endl ;
    uart4.ReadUntil (StringCallback);
    std::cout << "___________read lines Basic___________" << std::endl ;
    uart4.ReadUntil (BasicCallback);
    
    std::cout << "___________read until String___________" << std::endl ;
    uart4.ReadUntil ( StringUntil, "]");
    
    std::cout << "___________read until basic___________" << std::endl ;
    uart4.ReadUntil(BasicUntil, "Beagle");
    
    uart4.Close();
    std::cout << "_______________END___________" << std::endl ;
}
