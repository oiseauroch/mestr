#include "Serial.h"
#include <boost/bind/bind.hpp>


/**
* @brief table with all uarts ports for beagleBone Black. Indices are related to UART enum
* 
*/
uart_t Serial::tableUart[] = {
    {"uart1", ConfigPin::P9_26, ConfigPin::P9_24, ConfigPin::P9_20, ConfigPin::P9_19, "/dev/ttyO1" },
    {"uart2", ConfigPin::P9_22, ConfigPin::P9_21, ConfigPin::INVALID, ConfigPin::INVALID, "/dev/ttyO2" },
    {"uart3", ConfigPin::INVALID, ConfigPin::P9_42, ConfigPin::P8_36, ConfigPin::P8_34, "/dev/ttyO3" },
    {"uart4", ConfigPin::P9_11, ConfigPin::P9_13, ConfigPin::P8_35, ConfigPin::P8_33, "/dev/ttyO4" },
    {"uart5", ConfigPin::P8_38, ConfigPin::P8_37, ConfigPin::P8_31, ConfigPin::P8_32, "/dev/ttyO1" },
    
};

/**
 * @brief serial constructor call the member function open()
 * 
 * @param uart uart chosen
 * @param baud_rate speed of the serial port
 * @param parity defualt value : none
 * @param char_size_o default value : 8
 * @param flow_ctrl_o default value : none
 * @param stop_bits_o default value : 1
 */
Serial::Serial(const Serial::UARTS uart, unsigned int baudRate, 
                            boost::asio::serial_port_base::parity parity,
                            boost::asio::serial_port_base::character_size charSize,
                            boost::asio::serial_port_base::flow_control flowCtrl,
                            boost::asio::serial_port_base::stop_bits stopBits)
: io(), port(io), timer(io), displayOutput(false), globalTimeout(true),
timeout(boost::posix_time::time_duration(boost::posix_time::seconds(0)))
{
    
    Open(uart, baudRate, parity, charSize, flowCtrl, stopBits);
}

/**
 * @brief open the serial port
 * 
 * @param uart uart chosen
 * @param baud_rate port speed
 * @param parity default value : none
 * @param char_size_o default value : 8
 * @param flow_ctrl_o default value : none
 * @param stop_bits_o default value : 1
 */
void Serial::Open(const Serial::UARTS uart, unsigned int baudRate,
                        boost::asio::serial_port_base::parity parity, 
                        boost::asio::serial_port_base::character_size charSize, 
                        boost::asio::serial_port_base::flow_control flowCtrl, 
                        boost::asio::serial_port_base::stop_bits stopBits) 
{
    uart_t uartPort = tableUart[uart];
    try {
        // set ports to uart mode
        ConfigPin::setPinMode(uartPort.rxd, ConfigPin::UART);
        ConfigPin::setPinMode(uartPort.txd, ConfigPin::UART);
    } catch (ConfigPin::BadPinConfiguration& e)
    {
        std::cout << "Unable to open " << uartPort.name << std::endl ;
        throw ;
    }
    this->port.open(uartPort.tty,this->error);
    this->port.set_option(boost::asio::serial_port_base::baud_rate( baudRate ));
    this->port.set_option(parity);
    this->port.set_option( charSize );
    this->port.set_option( flowCtrl );
    this->port.set_option( stopBits );
    this->portOpen = true;
}

/**
 * @brief close serial port previously open
 * 
 */
void Serial::Close()
{
    port.close();
    this->portOpen = false;
}

/**
 * @brief specifiy the duration of deadline timer
 * 
 * @param dur duration of the delay
 * @param global choose if timer is launched only at the beginning (true)  or refreshed for each line read (false)
 */
void Serial::SetTimeout(int dur, bool global)
{
    this->SetTimeout(boost::posix_time::time_duration(boost::posix_time::seconds(dur)), global);
}

/**
 * @brief specifiy the duration of deadline timer
 * 
 * @param dur duration of the delay
 * @param global choose if timer is launched only at the beginning (true)  or refreshed for each line read (false)
 */
void Serial::SetTimeout(boost::posix_time::time_duration dur, bool global)
{
    this->timeout = dur;
    this->globalTimeout = global;
}

/**
 * @brief print output to the standart one (default false)
 * 
 * @param display redirect (true) or not (false)
 */
void Serial::SetDisplay(bool display)
{
    this->displayOutput = display;
}

/**
* @brief read until the callback function return false. Callback is call every time "delimiter" is found (default "\n" )
* 
* @param delimiter delimiter for calling callback function
* @param cllbck : callback function with type : bool foo(boost::asio::streambuf& buf, const boost::system::error_code& error, std::size_t nbBytes).
* 
* exemple : 
* bool BasicCallback(boost::asio::streambuf& buf, const boost::system::error_code& error, std::size_t nbBytes)
*{ 
*    if(!error) 
*    { 
*        std::string s(boost::asio::buffers_begin(buf.data()),boost::asio::buffers_begin(buf.data()) + nbBytes);
*        buf.consume(nbBytes);
*       
*         ...do what you want with the string...
*            
*        again?return true:return false;
*    } else {
*        std::cout << "read error : " << error << " "  << error.message() << std::endl ;
*        throw(boost::system::system_error(boost::system::error_code(error), "error while reading"));
*   }
*}
*/
void Serial::ReadUntil (basicCallback cllbck, std::string delimiter)
{
    
    this->basic = true;
    this->delimiter = delimiter;
    this->callback->basC = cllbck;
    this->_ReadUntil();
}

/**
 * @brief  read until the callback function return false. Callback is call every time "delimiter" is found (default "\n" )
* 
* @param cllbck callback with prototype like bool foo(const std::string& str)
* @param delimiter delimiter for calling callback ( default '\n' )
*/
void Serial::ReadUntil (stringCallback cllbck, std::string delimiter)
{
    this->basic = false;
    this->delimiter = delimiter;
    this->callback->strC = cllbck;
    this->_ReadUntil();
}
/************************************/
/*          PRIVATE FUNCTIONS             */
/***********************************/ 

/**
 * @brief  begin the deadline timer
 * 
 */
void Serial::_StartTimer()
{
    if ( this->timeout != boost::posix_time::seconds(0))
    {
        this->timer.expires_from_now(boost::posix_time::time_duration(this->timeout));
        this->timer.async_wait(boost::bind(&Serial::_TimeoutExpired, this,  boost::asio::placeholders::error));    
    }
}

/**
* @brief launch read process. 
* 
* @throw std::errc::bad_file_descriptor "port not open"
* 
*/
void Serial::_ReadUntil()
{
#ifdef BOOST_LT_1_66
    io.reset();
#else
    io.restart();
#endif    
    if(this->portOpen)
    {
        boost::asio::async_read_until(this->port,this->buf, this->delimiter, boost::bind(&Serial::_ReadCallback, this,  boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
        this->_StartTimer();
        io.run();
    } else 
    {
        throw(std::system_error(std::make_error_code(std::errc::bad_file_descriptor), "port not open"));
    }
}


/**
 * @brief callback function of the timer
 * 
 * @param error 
 * 
 * @throw TimeoutException timer finish before reading process ended
 */
void Serial::_TimeoutExpired(boost::system::error_code error)
{
    if (error != boost::asio::error::operation_aborted)  // error get by restarting timer
    {
        this->io.stop();
        throw(TimeoutException("Timeout expired"));
    } 
}




/**
 * @brief Callback function of the reader depending which function was read will read the buffer and call the user callback with a string or directly call the user callback
 * Will also print the output and restart the timer if specified
 * 
 * @param error 
 * @param nb_bytes 
 */
void Serial::_ReadCallback (const boost::system::error_code& error, std::size_t nbBytes)
{
    if(error && error.value() == boost::system::errc::operation_canceled) {
        return ; // deadline timer TODO add temporarly read management
    } else 
    {
        bool restart = false;
        if(this->basic == 1)  // callback with buffer
        {
            restart = (*Serial::callback->basC)(this->buf, error, nbBytes);
        } else // calback with string
        {
            if(!error)
            {
                std::string s(boost::asio::buffers_begin(buf.data()),
                                boost::asio::buffers_begin(buf.data()) + nbBytes);
                  buf.consume(nbBytes);
            
                // print output if specified
                if (this->displayOutput) { std::cout << s << std::endl ; }
                
                // restart timer if specified
                if(!globalTimeout) { this->_StartTimer(); }
                
            restart = (*Serial::callback->strC)(s);
            
            } else if (error.value() != boost::system::errc::operation_canceled)  // error due to deadline timer callback
            {
                std::cout << "read error : " << error << " "  << error.message() << std::endl ;
                throw(boost::system::system_error(boost::system::error_code(error), "error while reading"));
            }
        }
        if (restart) {
        boost::asio::async_read_until(this->port,this->buf, this->delimiter, boost::bind(&Serial::_ReadCallback, this,  boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
        } else 
        {
            this->timer.cancel();
        }
    }
}

