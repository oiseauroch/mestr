#ifndef SERIAL_H
#define SERIAL_H
#include <iostream>
#include <boost/asio.hpp>
#include <boost/asio/serial_port.hpp>
#include <boost/asio/read_until.hpp>
#include <regex>
#include <map>
#include <stdexcept>
#include "ConfigPin.h"

/**
* @brief structure that define an uart port. TODO cts and rts are currently not usable
* 
*/
typedef struct uart_t {
    std::string name ;
    ConfigPin::PINS rxd ;
    ConfigPin::PINS  txd ;
    ConfigPin::PINS cts ;
    ConfigPin::PINS  rts ;
    std::string tty;
    
} uart_t;


typedef bool (* stringCallback)(const std::string& str);
typedef bool (* basicCallback)(boost::asio::streambuf& buf, const boost::system::error_code& error, std::size_t nbBytes);

union callback_t {
    stringCallback strC;
    basicCallback basC;
} ;


class TimeoutException: public std::runtime_error
{
public:
    TimeoutException(const std::string& arg): runtime_error(arg) {}
};

/**
 * @brief class for parsing serial output 
 * 
 */
class Serial
{
    
public :
    enum UARTS {
        UART1,
        UART2,
        UART3,
        UART4,
        UART5
    };
    
    Serial();
    
    Serial ( const UARTS uart, unsigned int baudRate,
                   boost::asio::serial_port_base::parity parity=
                   boost::asio::serial_port_base::parity ( boost::asio::serial_port_base::parity::none ),
                   boost::asio::serial_port_base::character_size charSize=
                   boost::asio::serial_port_base::character_size ( 8 ),
                   boost::asio::serial_port_base::flow_control flowCtrl=
                   boost::asio::serial_port_base::flow_control(),
                   boost::asio::serial_port_base::stop_bits stopBits=
                   boost::asio::serial_port_base::stop_bits() );
    
    void  Open ( const UARTS uart, unsigned int baudRate,
                 boost::asio::serial_port_base::parity parity=
                 boost::asio::serial_port_base::parity ( boost::asio::serial_port_base::parity::none ),
                 boost::asio::serial_port_base::character_size charSize=
                 boost::asio::serial_port_base::character_size ( 8 ),
                 boost::asio::serial_port_base::flow_control flowCtrl=
                 boost::asio::serial_port_base::flow_control(),
                 boost::asio::serial_port_base::stop_bits stopBits=
                 boost::asio::serial_port_base::stop_bits() );
    
    
    void Close();
 
    
    void ReadUntil (stringCallback callback , std::string delimiter = "\n");
    void ReadUntil (basicCallback callback, std::string delimiter = "\n" );
    
    
    void SetTimeout(boost::posix_time::time_duration dur, bool global = true);
    void SetTimeout(int dur, bool global= true);
    void SetDisplay(bool display);
    
    
private :
    static uart_t tableUart[5];
    callback_t* callback;
    std::string  delimiter;
    boost::asio::io_service io;
    boost::asio::serial_port port;
    boost::asio::streambuf buf;
    boost::system::error_code error;
    boost::asio::deadline_timer timer;
    bool displayOutput;
    bool globalTimeout;
    bool portOpen;
    bool basic; 
    
    boost::posix_time::time_duration timeout;
    bool IsOpen();
    void _ReadCallback ( const boost::system::error_code& error, std::size_t nbBytes );
    void _StartTimer();
    void _TimeoutExpired(boost::system::error_code error);
    void _ReadUntil();
};
#endif
