#ifndef CONFIG_PIN_H
#define CONFIG_PIN_H

#define GPIO_PATH "/sys/class/gpio/gpio"
#define BASH_CMD "config-pin "

#include <sstream>
#include <fstream>
#include <iostream>

namespace ConfigPin {
    

    /**
    * @brief enum for pins of the beaglbone black.
    * default modes are 
    *  - default,
    *  - gpio,
    *  - gpio_pu,
    *  - gpio_pd
    *  - gpio_input
    *  - output
    *  extra mode are defined for each pin in the enum declaration
    */
    enum PINS {
        P8_03= 38 , // extra modes  : 
        P8_04= 39 , // extra modes : 
        P8_05= 34 , // extra modes : 
        P8_06= 35 , // extra modes : 
        P8_07= 66 , // extra modes :  timer
        P8_08= 67 , // extra modes :  timer
        P8_09= 69 , // extra modes :  timer
        P8_10= 68 , // extra modes :  timer
        P8_11= 45 , // extra modes :  qep pruout
        P8_12= 44 , // extra modes :  qep pruout
        P8_13= 23 , // extra modes :  pwm
        P8_14= 26 , // extra modes :  pwm
        P8_15= 47 , // extra modes :  qep pru_ecap pruin
        P8_16= 46 , // extra modes :  qep pruin
        P8_17= 27 , // extra modes :  pwm
        P8_18= 65 , // extra modes : 
        P8_19= 22 , // extra modes :  pwm
        P8_20= 63 , // extra modes :  pruout pruin
        P8_21= 62 , // extra modes :  pruout pruin
        P8_22= 37 , // extra modes : 
        P8_23= 36 , // extra modes : 
        P8_24= 33 , // extra modes : 
        P8_25= 32 , // extra modes : 
        P8_26= 61 , // extra modes : 
        P8_27= 86 , // extra modes :  pruout pruin
        P8_28= 88 , // extra modes :  pruout pruin
        P8_29= 87 ,   // extra modes :  pruout pruin
        P8_30= 89 ,  // extra modes :  pruout pruin
        P8_31= 10 ,   // extra modes :  uart qep
        P8_32= 11 ,   // extra modes :  qep
        P8_33=  9 ,   // extra modes :  qep
        P8_34= 81 ,  // extra modes :  pwm
        P8_35=  8 ,   // extra modes :  qep
        P8_36= 80 , // extra modes :  pwm
        P8_37= 78 , // extra modes :  uart pwm
        P8_38= 79 , // extra modes :  uart pwm
        P8_39= 76 , // extra modes :  qep pruout pruin
        P8_40= 77 , // extra modes :  qep pruout pruin
        P8_41= 74 , // extra modes :  qep pruout pruin
        P8_42= 75 , // extra modes :  qep pruout pruin
        P8_43= 72 , // extra modes :  pwm pruout pruin
        P8_44= 73 , // extra modes :  pwm pruout pruin
        P8_45= 70 , // extra modes :  pwm pruout pruin
        P8_46= 71 , //  extra modes :  pwm pruout pruin
        P9_11= 30 , // extra modes  : uart
        P9_12= 60 , // extra modes  :
        P9_14= 50 , // extra modes  : uart
        P9_13= 31 , // extra modes  : pwm
        P9_15= 48 , // extra modes  : pwm
        P9_16= 51 , // extra modes  : pwm
        P9_17=  5 , // extra modes  : spi_cs i2c pwm pru_uart
        P9_18=  4 , // extra modes  : spi i2c pwm pru_uart
        P9_19= 13 , // extra modes  : spi_cs can i2c pru_uart timer
        P9_20= 12 , // extra modes  : spi_cs can i2c pru_uart timer
        P9_21=  3 , // extra modes  : spi uart i2c pwm pru_uart
        P9_22=  2 , // extra modes  : spi_sclk uart i2c pwm pru_uart
        P9_23= 49 , // extra modes  : pwm
        P9_24= 15 , // extra modes  : uart can i2c pru_uart pruin
        P9_25=117 , // extra modes  : qep pruout pruin
        P9_26= 14 , // extra modes  : uart can i2c pru_uart pruin
        P9_27=115 , // extra modes  : qep pruout pruin
        P9_28=113 , // extra modes  : spi_cs pwm pwm2 pruout pruin
        P9_29=111 , // extra modes  : spi pwm pruout pruin
        P9_30=112 , // extra modes  : spi pwm pruout pruin
        P9_31=110 , // extra modes  : spi_sclk pwm pruout pruin
        P9_41= 20 , // extra modes  : timer pruin
        P9_42=  7 ,  // extra modes  : spi_cs spi_sclk uart pwm pru_ecap 
        INVALID=0 // for inexistant pin
        
    };
    

    enum PIN_VALUE {
        HIGH = 1,
        LOW = 0
    };
    

    
    /**
    * @brief all mode potentially available for pins
    * 
    */
    enum PIN_MODE { 
        CAN,
        DEFAULT,
        GPIO,
        GPIO_INPUT,
        GPIO_PD,
        GPIO_PU,
        I2C,
        OUTPUT,
        PRU_ECAP,
        PRUIN,
        PRUOUT,
        PRU_UART,
        PWM,
        PWM2,
        QEP,
        SPI,
        SPI_CS,
        SPI_SCLK,
        TIMER,
        UART
    };
    


    
    /**
    * @brief error thrown by configpin in case of problems
    * 
    */
    class BadPinConfiguration: public std::runtime_error
    {
    public:
        explicit BadPinConfiguration(const std::string& arg): runtime_error(arg) {}
    };
    
    void writePin(PINS pin, PIN_VALUE value);
    int readPin(PINS pin);
    void  setPinMode(PINS pin, PIN_MODE mode);

    std::ostream& operator<<(std::ostream& os, PIN_MODE pinMode);
    std::ostream& operator<<(std::ostream& os, PIN_VALUE pinValue);
    std::ostream& operator<<(std::ostream& os, PINS pin);
    
}
#endif // CONFIG_PIN_H
