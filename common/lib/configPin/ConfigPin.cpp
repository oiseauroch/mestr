#include "ConfigPin.h"


namespace ConfigPin {
    
    /**
    * @brief set pin to specific mode
    * this function use config-pin to do so 
    * 
    * @param pin 
    * @param mode 
    * @throw BadPinConfiguration if mode not available or pin used by an other cape
    */
    void  setPinMode(PINS pin, PIN_MODE mode)
    {
        std::stringstream ss;
        ss << BASH_CMD << pin << " " << mode ;
        
        int err_code = std::system(ss.str().c_str());
        if( err_code != 0)
        {
            throw BadPinConfiguration("config not available or pin managed by an other cape");
        }
    }

    /**
    * @brief read pin value
    * this function is not using config-pin but read directly the value in GPIO_PATH
    * 
    * @param pin 
    * @return value of pin
    * @throw BadPinConfiguration if unable to read pin
    */
    int readPin(PINS pin)
    {
        std::string filename = GPIO_PATH + std::to_string(pin) + "/value" ;
        
        std::fstream s(filename,  s.in );
        if (!s.is_open()) {
            throw BadPinConfiguration("unable to read pin " + std::to_string(pin));
        } else {
            int v;
            s >> v;
            return v;
        }
    }

    /**
    * @brief write value to a pin
    * this function uses config-pin to do so
    * 
    * @param pin 
    * @param value 
    * @throw BadPinConfiguration if unable to write
    */
    void writePin(PINS pin, PIN_VALUE value)
    {
        std::stringstream ss;
        ss << BASH_CMD << pin << " " << value << std::endl ;
        int err_code = std::system(ss.str().c_str());
        if( err_code != 0)
        {
            throw BadPinConfiguration("pin managed by an other cape");
        }
    }

    /**
    * @brief operator overloading 
    * 
    * @param os 
    * @param pinMode 
    * @return std::ostream&
    */
    std::ostream& operator<<(std::ostream& os, PIN_MODE pinMode)
    {
        switch(pinMode)
        {
            case  CAN  : os << "can";    break;
            case DEFAULT: os << "default"; break;
            case GPIO : os << "gpio";  break;
            case GPIO_INPUT  : os << "gpio_input";   break;
            case GPIO_PD : os << "gpio_pd"; break;
            case GPIO_PU : os << "gpio_pu"; break;
            case I2C : os << "i2c"; break;
            case OUTPUT : os << "output"; break;
            case PRU_ECAP : os << "pru_ecap"; break;
            case PRU_UART : os << "pru_uart"; break;
            case PRUIN : os << "pruin"; break;
            case PRUOUT : os << "pruout"; break;
            case PWM : os << "pwm"; break;
            case PWM2 : os << "pwm2"; break;
            case QEP : os << "qep"; break;
            case SPI : os << "spi"; break;
            case SPI_CS : os << "spi_cs"; break;
            case SPI_SCLK : os << "spi_sclk"; break;
            case TIMER : os << "timer"; break;
            case UART : os << "uart"; break;
            default    : os.setstate(std::ios_base::failbit);
        }
        return os;
    }

    /**
     * @brief operator overloading 
    * 
    * @param os 
    * @param pinValue 
    * @return std::ostream&
    */
    std::ostream& operator<<(std::ostream& os, PIN_VALUE pinValue)
    {
        switch(pinValue)
        {
            case HIGH : os << "high" ; break;
            case LOW : os << "low" ; break;
            default    : os.setstate(std::ios_base::failbit);
        }
        return os;
    }

    /**
     * @brief operator overloading 
    * 
    * @param os 
    * @param pin 
    * @return std::ostream&
    */
    std::ostream& operator<<(std::ostream& os, PINS pin)
    {
        switch(pin)
        {
            case P8_03 : os << "P8_3" ; break ;
            case P8_04 : os << "P8_04" ; break ;
            case P8_05 : os << "P8_05" ; break ;
            case P8_06 : os << "P8_06" ; break ;
            case P8_07 : os << "P8_07" ; break ;
            case P8_08 : os << "P8_08" ; break ;
            case P8_09 : os << "P8_09" ; break ;
            case P8_10 : os << "P8_10" ; break ;
            case P8_11 : os << "P8_11" ; break ;
            case P8_12 : os << "P8_12" ; break ;
            case P8_13 : os << "P8_13" ; break ;
            case P8_14 : os << "P8_14" ; break ;
            case P8_15 : os << "P8_15" ; break ;
            case P8_16 : os << "P8_16" ; break ;
            case P8_17 : os << "P8_17" ; break ;
            case P8_18 : os << "P8_18" ; break ;
            case P8_19 : os << "P8_19" ; break ;
            case P8_20 : os << "P8_20" ; break ;
            case P8_21 : os << "P8_21" ; break ;
            case P8_22 : os << "P8_22" ; break ;
            case P8_23 : os << "P8_23" ; break ;
            case P8_24 : os << "P8_24" ; break ;
            case P8_25 : os << "P8_25" ; break ;
            case P8_26 : os << "P8_26" ; break ;
            case P8_27 : os << "P8_27" ; break ;
            case P8_28 : os << "P8_28" ; break ;
            case P8_29 : os << "P8_29" ; break ;
            case P8_30 : os << "P8_30" ; break ;
            case P8_31 : os << "P8_31" ; break ;
            case P8_32 : os << "P8_32" ; break ;
            case P8_33 : os << "P8_33" ; break ;
            case P8_34 : os << "P8_34" ; break ;
            case P8_35 : os << "P8_35" ; break ;
            case P8_36 : os << "P8_36" ; break ;
            case P8_37 : os << "P8_37" ; break ;
            case P8_38 : os << "P8_38" ; break ;
            case P8_39 : os << "P8_39" ; break ;
            case P8_40 : os << "P8_40" ; break ;
            case P8_41 : os << "P8_41" ; break ;
            case P8_42 : os << "P8_42" ; break ;
            case P8_43 : os << "P8_43" ; break ;
            case P8_44 : os << "P8_44" ; break ;
            case P8_45 : os << "P8_45" ; break ;
            case P8_46 : os << "P8_46" ; break ;
            case P9_11 : os << "P9_11" ; break ;
            case P9_12 : os << "P9_12" ; break ;
            case P9_13 : os << "P9_13" ; break ;
            case P9_14 : os << "P9_14" ; break ;
            case P9_15 : os << "P9_15" ; break ;
            case P9_16 : os << "P9_16" ; break ;
            case P9_17 : os << "P9_17" ; break ;
            case P9_18 : os << "P9_18" ; break ;
            case P9_19 : os << "P9_19" ; break ;
            case P9_20 : os << "P9_20" ; break ;
            case P9_21 : os << "P9_21" ; break ;
            case P9_22 : os << "P9_22" ; break ;
            case P9_23 : os << "P9_23" ; break ;
            case P9_24 : os << "P9_24" ; break ;
            case P9_25 : os << "P9_25" ; break ;
            case P9_26 : os << "P9_26" ; break ;
            case P9_27 : os << "P9_27" ; break ;
            case P9_28 : os << "P9_28" ; break ;
            case P9_29 : os << "P9_29" ; break ;
            case P9_30 : os << "P9_30" ; break ;
            case P9_31 : os << "P9_31" ; break ;
            case P9_41 : os << "P9_41" ; break ;
            case P9_42 : os << "P9_42" ; break ;
            default    : os.setstate(std::ios_base::failbit);
        }
        return os;
    }
}
