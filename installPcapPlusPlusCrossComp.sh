#!/bin/bash

is_root()
{
    [[ "$EUID" -eq 0 ]]
}


git clone https://github.com/seladb/PcapPlusPlus.git

CXX="arm-linux-gnueabihf-g++"
C="arm-linux-gnueabihf-gcc"
AR="arm-linux-gnueabihf-ar"
pathPcppMk="mk\/PcapPlusPlus.mk.linuxarm"
pathPlatformMk="mk\/platform.mk.linuxarm"
pathNetinet="-I\/usr\/arm-linux-gnueabihf\/include\/netinet"
installDir="/usr/arm-linux-gnueabihf/"
sed -e "/CXX := g++/ s/g++/${CXX}/" -e "/CC := gcc/ s/gcc/${C}/" -e "/AR := ar/ s/ar/${AR}/" "PcapPlusPlus/mk/platform.mk.linux" > "PcapPlusPlus/mk/platform.mk.linuxarm"
sed -e "s/-I\/usr\/include\/netinet/${pathNetinet}/" "PcapPlusPlus/mk/PcapPlusPlus.mk.linux" > "PcapPlusPlus/mk/PcapPlusPlus.mk.linuxarm"
sed -e "s/mk\/PcapPlusPlus.mk.linux/${pathPcppMk}/" -e "s/mk\/platform.mk.linux/${pathPlatformMk}/" "PcapPlusPlus/configure-linux.sh" > "PcapPlusPlus/configure-linuxarm.sh"
sed -e "/# copy examples/ s/# copy examples/# copy examples \nif [ -d examples ]; then/" -e '/cp examples\/\* $INSTALL_DIR\/bin/ s/$INSTALL_DIR\/bin/$INSTALL_DIR\/bin \nfi/' -e "/# create template makefile/ s/makefile/makefile \nif [ -d mk ]; then/" -e '/mv PcapPlusPlus.pc $PKG_CONFIG_PATH/ s/$PKG_CONFIG_PATH/$PKG_CONFIG_PATH \nfi/' "PcapPlusPlus/mk/install.sh.template" > "PcapPlusPlus/mk/install.sh.template2"

mv "PcapPlusPlus/mk/install.sh.template2" "PcapPlusPlus/mk/install.sh.template"
cd PcapPlusPlus
chmod +x configure-linuxarm.sh
./configure-linuxarm.sh --use-immediate-mode --install-dir "$installDir"

make libs

is_root && make install || sudo make install

cd ..
rm -rf PcapPlusPlus
